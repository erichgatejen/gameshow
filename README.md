**ABOUT**
This was used with a specific game project I did.  If I had the time, I would share
the hardware specs and how it was put together, but it really isn't anything 
special.   This was mostly a bridge between Ulimarc input controllers and 
LED outputs.  I had to reverse engineer a lot of things to get it working, so
I figured it might have some utility to look at.


**PACDRIVE**
The full package can be found here.


**NOTES**
Make sure the PacDrive.dll is somewhere in the dll search path.

